+++
title = 'À propos'
date = 2024-01-29T23:41:38+01:00
draft = true
+++

Je suis un développeur passionnée par ce qu'il fait, ce qu'il apprend et ce qu'il peut transmettre.

J'ai comme hobbies l’amélioration de mes connaissances dans les sujets qui me plaisent et qui m'attire que ce soit
dans les jeux vidéo, la lecture ou dans les films et séries d'animations.

Avec une grande philosophie sur la vie privée et les essais d'héberger soit même des applications, services, et serveurs
de jeux. Allant à l'hébergement d'un vpn ([wireguard](https://www.wireguard.com/)), ou d'autres applications comme
[adguard](https://adguard.com/fr/adguard-home/overview.html) pour le serveur DHCP et l'adblock,
[seafile](https://www.seafile.com/en/home/) pour les documents et pleins d'autres.